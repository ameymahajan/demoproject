class CreatePosts < ActiveRecord::Migration[5.2]
  def change
    create_table :posts do |t|
      t.belongs_to :user, index: true
      t.integer :post_mode, default: 0
      t.timestamps
    end
  end
end

Rails.application.routes.draw do
  resources :posts do
  	resources :comments
  end 
  
  resources :posts do
  	resources :likes
  end

  resources :comments do
  	resources :likes
  end
  
  resources :friendships
  
  root 'dashboard#index'
  get 'welcome/index'
  # post 'friendships/index'
  # post 'friendships/create'
  # post 'friendships/update'
  devise_for :users, controllers: {registrations: 'registrations'}
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end

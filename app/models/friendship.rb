class Friendship < ApplicationRecord
  belongs_to :user
  belongs_to :friend, :class_name => "User" 

  scope :pending_friends, -> (id) {where(friend_id: id)} 
  before_create :set_status
  # after_update :update_reverse_friendship

  private 
  def set_status
    self.status = 0 
  end


  # def update_reverse_friendship
    # reverse_friendship = Friendship.where(
    #   user_id: self.friend_id,
    #   friend_id: self.user_id
    # ).first
    
  #   reverse_friendship.update(status: 1) if reverse_friendship.present?
  # end
end

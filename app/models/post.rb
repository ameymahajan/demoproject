class Post < ApplicationRecord
  belongs_to :user
  has_many :comments
  has_many :likes, as: :likeable
  has_one_attached :file
  validates :post_mode,:caption, presence: true 
  validates :caption, length: { minimum: 5, maximum: 100}
end
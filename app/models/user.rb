class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable
  has_many :posts
  has_many :friendships 
  has_many :friends, :through => :friendships
  
  validates :username,:first_name,:last_name,:age, presence: true  
  validates :username, uniqueness: true, length: {minimum: 3}
  validates :age, numericality: true

  scope :new_friends, -> (fname) { includes(:friendships).where(first_name: fname) }  
end

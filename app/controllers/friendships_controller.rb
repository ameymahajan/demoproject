class FriendshipsController < ApplicationController
  before_action :find_friendship

  def index
    @users = User.new_friends(params[:user][:first_name])
    @pending = Friendship.pending_friends(current_user.id)
  end

  def create
    @friendship_user = current_user.friendships.new(friend_id: params[:friend_id])
    @friendship_friend  = User.find_by(id: params[:friend_id]).friendships.new(friend_id: current_user.id)
    if @friendship_user.save && @friendship_friend.save 
      redirect_to root_path
    else
      render plain: "You cannot send Friend Request"
    end
  end
  
  def update
    if @friendship.present? && @friendship.update(status: 1)
      update_reverse_friendship
      redirect_to root_path
    else
      render plain: "Already Friends"
    end
  end

  private

  def find_friendship
    @friendship = Friendship.find_by(id: params[:id])
  end

  def update_reverse_friendship
    reverse_friendship = Friendship.where(
      user_id: @friendship.friend_id,
      friend_id: @friendship.user_id
    ).first

    if reverse_friendship.present?
      reverse_friendship.update(status: 1)
    else 
      render plain: "Already Friends"
    end
  end
end

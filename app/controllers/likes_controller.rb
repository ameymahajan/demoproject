class LikesController < ApplicationController
  before_action :set_post,:set_comment
  
  def index
    @like = Like.all
  end
    
  def create
    if params[:post_id]
      @like = @post.likes.new
      @like.user_id = current_user.id
      @like.username = current_user.username
    else
      @like = @comment.likes.new
      @like.user_id = current_user.id
      @like.username = current_user.username
    end

    if @like.save
      respond_to do |format|
        format.js
        format.html {redirect_to root_path}
      end
    end  
  end

  def destroy
    @unlike = Like.find_by(id: params[:id])
    if @unlike.destroy
      respond_to do |format|
        format.js
        format.html {redirect_to root_path}
      end
    end
  end
  private
    def set_post
      @post = Post.find_by(id: params[:post_id])
    end

    def set_comment
      @comment = Comment.find_by(id: params[:comment_id])
    end
end

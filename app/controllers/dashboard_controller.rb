class DashboardController < ApplicationController
  def index
    @post = Post.new
    if user_signed_in?
      @posts   = current_user.posts
      @friends = current_user.friendships.where(status: 1)
    else
      redirect_to welcome_index_path
    end
  end
end

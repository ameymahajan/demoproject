class RegistrationsController < Devise::RegistrationsController
	self.per_form_csrf_tokens = true
  def sign_up_params
    params.require(:user).permit(:username,:email,:password,:encrypted_password,:first_name,:last_name,:age,:city )    
  end 
end

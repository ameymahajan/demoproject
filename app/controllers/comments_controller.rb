class CommentsController < ApplicationController
  before_action :find_post
  
  def index
    @comment = @post.comments
  end
  def create
    @comment = @post.comments.new(comment_params)
    respond_to do |format|
      if @comment.save
        format.js      
        format.html { redirect_to root_path, notice: 'Commented.' } 
      end
    end
  end

  def destroy
    @comment = current_user.posts.find_by(id: params[:post_id]).comments.find_by(id: params[:id])
    respond_to do |format|
      if @comment.destroy
        format.html {redirect_to root_path}
        format.js
      end
    end
  end

  private
  def comment_params 
    params.require(:comment).permit(:comment).merge(user_id: current_user.id)
  end

  def find_post
    @post = Post.find_by(id: params[:post_id])
  end
end 